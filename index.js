const express = require('express');
const app = express();
var bodyParser = require('body-parser')
const FOREST_ENV_SECRET="4e8259f3a3a648ee7222844fe01474a87eaa1f06ec3db4f5174cf61f2f31dfeb";
const FOREST_AUTH_SECRET="2cf8b59294d5caac41ef0caab941771e6e658f8a14b8d95a";
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/shoom');
var db = mongoose.connection;

const scrape = require('./scrape');

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!

  console.log("Database Online! ==================================================== = = = = = = = = ");
});


var CronJob = require('cron').CronJob;
const scrapeCron = new CronJob('1 1 * * *', function() {
  console.log('----------------------------------------')
  console.log('------------- RUNNING CRON ------------')
  console.log('----------------------------------------')

  scrape.StartScrape();

}, null, true, 'America/Los_Angeles');

var Item = require('./models/model');

app.use(require('forest-express-mongoose').init({
    modelsDir: __dirname + '/models',
    envSecret: FOREST_ENV_SECRET,
    authSecret: FOREST_AUTH_SECRET,
    mongoose: require('mongoose'),
  }));


  var nextCronRun = scrapeCron.nextDates(5);

app.get('/', function(req,res){
    Item.count({}, function (err, count){
      res.send({"shoom":count,
      "/start":"to start scrape forcefully",
      "nextscrape UTC next 5": nextCronRun,
      "/all" : "view all entries at once."
      })
    })
});


app.get('/all', function (req,res){
  Item.find({}, function (err,docs){
    res.send(docs)
  });
});

app.get('/start', function(req,res){

  scrape.StartScrape();

  res.send({"shoom":"Starting scrape now."})

});

app.listen(6969);