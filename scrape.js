const NewsAPI = require('newsapi');
const newsapi = new NewsAPI('898d9715cb084dc999537d6dd3da9824');
//898d9715cb084dc999537d6dd3da9824
//b2c4e7ccbb0e42f2a2e8ab07ad03ede0
//8f61f2f9e66e4223a4e79526586a907a
// getting-started.js
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/shoom');
const TaskRunner = require('concurrent-tasks');
const runner = new TaskRunner();

var Item = require('./models/model');

let d = new Date();
let yesterday = d.setDate(d.getDate() - 1);
yesterday = new Date(yesterday).toISOString();

let Brands = ["AceVPN","ActiVPN","AirVPN","AnonVPN","AnonymousVPN","Astrill","Avira Phantom VPN","Betternet","BolehVPN","CactusVPN","CrypticVPN","CyberGhost","Disconnect.me","Faceless.ME","Freedom-IP","FrootVPN","GooseVPN","Hide.me","HideMyAss","Hotspot Shield","IBVPN","IPVanish","IPinator","IVPN","Ivacy","KeepSolid VPN Unlimited","Kepard","LeVPN","LiquidVPN","Mullvad","NordVPN","OneVPN","ProXPN","SaferVPN","Smart DNS Proxy","StrongVPN","SurfEasy","TigerVPN","TorVPN","Torguard","TotalVPN","TunnelBear","Unblock VPN","Unblock-Us","UnoTelly","VikingVPN","VyprVPN","WiTopia","WindScribe","Zenmate","purevpn","surfshark","torguard","VPN"]


/* 
    RUN HERE
*/

module.exports = {
    StartScrape: (req,res,next) =>{
        runner.addMultiple(generateTasks());
        console.log(Brands.length + " Brands to scrape ~~~~~~~~~~~~~~~~~~~~ :O");

    }
}

//scrapeAll("purevpn");


/* 
    FUNCTIONS BELOW
*/

function generateTasks() {
    const tasks = [];
    Brands.forEach(function(Brand){
        tasks.push(done => {
            setTimeout(() => {
                scrapeAll(Brand);
                done();
            }, Math.random() * 5000)
        });
    })
    return tasks;
}

function scrapeAll(keyword){

    newsapi.v2.everything({
        q: keyword,
        language: 'en',
        from: yesterday,
        page_size:100,
        sort_by:'popularity'
      }).then(response => {
        
        Data = response.articles;
        
        var Data2 = Data.map(function(el){
            var o = Object.assign({}, el);
            o.keyword = keyword;
            return o;
        })
           
        Item.create(Data2, function(err,result){
            console.log(keyword + " DONE!!! ----------------------------");
        });
      });
}