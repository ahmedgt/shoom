let mongoose = require('mongoose');

// Article Schema
let dbSchema = mongoose.Schema({
    url:
    {type:String,unique:true},
    keyword:
    {
        type:String
    },
    publishedAt:{type:Date}
},
    
    { strict: false });

let Item = module.exports = mongoose.model('News', dbSchema, 'news');
